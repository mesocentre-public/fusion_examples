#PBS -S /bin/bash
#PBS -N hello_julia
#PBS -j oe
#PBS -l walltime=00:20:00
#PBS -l select=1:ncpus=1:mem=1gb
#PBS -P test

# Load the same modules as environment configuration
module load julia/1.1.0

# Go to the current directory 
cd $PBS_O_WORKDIR

# Run code
julia hello_julia.jl
