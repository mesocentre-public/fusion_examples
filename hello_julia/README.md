# Sample julia job

To configure julia environment (install required packages)

```shell
module load julia/1.1.0
julia configure_env.jl
```

To launch this example

```shell
qsub pbs.sh
```
