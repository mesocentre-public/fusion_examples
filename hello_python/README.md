# Sample python job

Configure the python environment.

```shell
module load anaconda3/5.3
conda create -n numpy-env
source activate numpy-env
conda install numpy
```

Submit the job to the scheduler.

```shell
qsub pbs.sh
```
