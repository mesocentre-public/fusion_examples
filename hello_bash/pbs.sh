#!/bin/bash

#PBS -l walltime=00:20:00
#PBS -l select=1:ncpus=1:mem=1gb
#PBS -N hello_bash
#PBS -j oe
#PBS -P test

# Move to directory where the job was submitted
cd $PBS_O_WORKDIR

# This script will be executed on remote server
# like a standard bash script
hostname
date
sleep 20
