program mpi_openmp_test
  implicit none
  include "mpif.h"
  integer :: ierr, proc_id, nprocs
  integer :: i, j
  integer :: thread_id, nthreads 
  integer :: omp_get_thread_num, omp_get_num_threads

  call MPI_INIT(ierr)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,nprocs,ierr)
  call MPI_COMM_RANK(MPI_COMM_WORLD,proc_id,ierr)
!$omp parallel private( thread_id )
  thread_id = omp_get_thread_num()
  nthreads = omp_get_num_threads()
  do i = 0, nprocs-1
     do j = 0, nthreads-1
        if ( proc_id == i .and. thread_id == j ) then
           write (*,*) "MPI rank:", proc_id, " with thread ID:", thread_id
        end if
     end do
  end do
!$omp end parallel
  call MPI_FINALIZE(ierr)
end program mpi_openmp_test
