# Sample MPI + OpenMP job

Compile this example.

```shell
module load intel-compilers/2019.1
module load intel-mpi/2019.1
make
```

Submit the job to the scheduler.

```
qsub pbs.sh
```
