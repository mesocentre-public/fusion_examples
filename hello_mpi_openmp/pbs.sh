#!/bin/bash

#PBS -N job-mpi-openmp-intel
#PBS -j oe
#PBS -l walltime=02:00:00
#PBS -l select=10:ncpus=24:mpiprocs=2:mem=12gb
#PBS -P test

# Load necessary modules
module purge
module load intel-compilers/2019.1
module load intel-mpi/2019.1

# Set OpenMP threads
export OMP_NUM_THREADS=12

# Move to directory where the job was submitted
cd $PBS_O_WORKDIR

# Run the OpenMP-MPI executable
mpirun -np 20 ./hello_mpi_openmp
