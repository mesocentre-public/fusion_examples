# Sample MPI job

Compile the example.

```shell
module load intel-compilers/2019.1
module load sgi-mpt/2.17
make
```

Submit the job to the scheduler

```shell
qsub pbs.sh
```
