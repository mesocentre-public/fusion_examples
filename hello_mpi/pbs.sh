#!/bin/bash

#PBS -l walltime=00:20:00
#PBS -l select=2:ncpus=24:mpiprocs=48
#PBS -N hello_mpi
#PBS -j oe
#PBS -P test

# Load necessary modules
module purge
module load intel-compilers/2019.1
module load sgi-mpt/2.17

# Move to directory where the job was submitted
cd $PBS_O_WORKDIR

# Run MPI script
mpiexec_mpt -np 96 ./hello_mpi 
