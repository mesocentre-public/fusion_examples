#!/bin/bash

#PBS -l walltime=00:20:00
#PBS -l select=1:ncpus=1:mem=1gb
#PBS -N hello_seq
#PBS -j oe
#PBS -P test

# Load necessary modules
module purge
module load intel-compilers/2019.1

# Move to directory where the job was submitted
cd $PBS_O_WORKDIR

# Run executable
./a.out
