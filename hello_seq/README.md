# Sample sequential job

Compile the C code.

```shell
module load intel-compilers/2019.1
icc hello_seq.c
```

Submit the code to the scheduler.

```shell 
qsub pbs.sh
```
