# Sample OpenMP job

Compile this example.

```shell
module load intel-compilers/2019.1
icc -qopenmp hello_omp.c
```

Submit the job to the scheduler.

```shell
qsub pbs.sh
```
