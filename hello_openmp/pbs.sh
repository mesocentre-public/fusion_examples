#!/bin/bash

#PBS -l walltime=00:20:00
#PBS -l select=1:ncpus=4:mem=2gb
#PBS -N hello_openmp
#PBS -j oe
#PBS -P test

# Load necessary modules
module purge
module load intel-compilers/2018.2

# Setup OpenMP threads
export OMP_NUM_THREADS=4

# Move to directory where the job was submitted
cd $PBS_O_WORKDIR

# Run OpenMP script
./a.out
