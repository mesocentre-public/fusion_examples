#!/bin/bash

#PBS -S /bin/bash
#PBS -N keras_cifar_train
#PBS -j oe
#PBS -l walltime=01:00:00
#PBS -l select=1:ncpus=24
#PBS -q gpuq
#PBS -P test

# Go to the directory where the job has been submitted 
cd $PBS_O_WORKDIR
[ ! -d output ] && mkdir output

# Module load 
module load anaconda3/5.3.1

# Activate anaconda environment code
source activate keras

# Train the network
python keras_cifar.py
