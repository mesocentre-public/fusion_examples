module load anaconda3/5.3.1
module load cuda/9.0
conda create --name keras
source activate keras
cd $PBS_O_WORKDIR
conda install cudatoolkit=8.0=3
conda install tensorflow-gpu
conda install keras
conda env export > config/environment.yml # save conda environment description
