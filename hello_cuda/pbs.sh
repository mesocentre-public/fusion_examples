#PBS -S /bin/bash

#PBS -l walltime=00:20:00
#PBS -l select=1:ncpus=1
#PBS -N hello_cuda
#PBS -j oe
#PBS -q gpuq
#PBS -P test

# Load necessary modules
module load cuda/9.1

# Move to directory where the job was submitted
cd $PBS_O_WORKDIR

# Compile cuda code - comment if you don't want to recompile
nvcc hello_cuda.cu

# Run cuda code
./a.out
